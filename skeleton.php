<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<body>
<?php 
function haha(int $num) {
	_haha($num, $num);
}
function _haha(int $num, int $startnum) {
	if($num > 0) {
      echo "<div style=\"font-size:" . $num . "px\">haha";
      for($i = $num; $i < $startnum; $i++) {
      	echo ".";
      }
      echo "</div>";
      _haha($num - 1, $startnum);
    } /* else {
    	echo "<h1>YEET</h1>";
    } */
}

// We PHP, boy
/*
$subject = "Baby";
echo "Hello ";
echo $subject;
echo "!<br>";
*/
haha(150);
?>
</body>
</html>
